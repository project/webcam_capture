/**
 * @file
 * Webcam module js library.
 */
window.webcam = {
  version: '1.0.9',

  // Globals.
  ie: !!navigator.userAgent.match(/MSIE/),
  protocol: location.protocol.match(/https/i) ? 'https' : 'http',
  callback: null, // User callback for completed uploads.
  swf_url: furl + 'webcam.swf', // URI to webcam.swf movie (defaults to cwd).
  shutter_url: furl + 'shutter.mp3', // URI to shutter.mp3 sound.
  api_url: '', // URL to upload script.
  loaded: false, // True when webcam movie finishes loading.
  quality: 90, // JPEG quality (1 - 100).
  shutter_sound: true, // Shutter sound effect on/off.
  stealth: false, // Stealth mode (do not freeze image upon capture).
  hooks: {
    onLoad: null,
    onComplete: null,
    onError: null
  }, // Callback hook functions.

  set_hook: function (name, callback) {
    // Set callback hook.
    // Supported hooks: onLoad, onComplete, onError.
    if ( typeof (this.hooks[name]) == 'undefined') {
      return alert('Hook type not supported: ' + name);
    }
    this.hooks[name] = callback;
  },

  fire_hook: function (name, value) {
    // Fire hook callback, passing optional value to it.
    if (this.hooks[name]) {
      if ( typeof (this.hooks[name]) == 'function') {
        // Callback is function reference, call directly.
        this.hooks[name](value);
      }
      else if ( typeof (this.hooks[name]) == 'array') {
        // Callback is PHP-style object instance method.
        this.hooks[name][0][this.hooks[name][1]](value);
      }
      else if (window[this.hooks[name]]) {
        // Callback is global function name.
        window[this.hooks[name]](value);
      }
      return true;
    }
    return false; // No hook defined.
  },

  set_api_url: function (url) {
    // Set location of upload API script.
    this.api_url = url;
  },

  set_swf_url: function (url) {
    // Set location of SWF movie (defaults to webcam.swf in cwd).
    this.swf_url = url;
  },

  get_html: function (width, height, server_width, server_height) {
    // Return HTML for embedding webcam capture movie.
    // Specify pixel width and height (640x480, 320x240, etc.).
    // Server width and height are optional, and default to movie width/height.
    if (!server_width) { server_width = width; }
    if (!server_height) { server_height = height; }

    var html = '';
    var flashvars = 'shutter_enabled=' + (this.shutter_sound ? 1 : 0) +
      '&shutter_url=' + escape(this.shutter_url) +
      '&width=' + width +
      '&height=' + height +
      '&server_width=' + server_width +
      '&server_height=' + server_height;

    if (this.ie) {
      html += '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="' + this.protocol + '://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="' + width + '" height="' + height + '" id="webcam_movie" align="middle"><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="false" /><param name="movie" value="' + this.swf_url + '" /><param name="loop" value="false" /><param name="menu" value="false" /><param name="quality" value="best" /><param name="bgcolor" value="#ffffff" /><param name="flashvars" value="' + flashvars + '"/></object>';
    }
    else {
      html += '<embed id="webcam_movie" src="' + this.swf_url + '" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="' + width + '" height="' + height + '" name="webcam_movie" align="middle" allowScriptAccess="always" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="' + flashvars + '" />';
    }

    this.loaded = false;
    return html;
  },

  get_movie: function () {
    // Get reference to movie object/embed in DOM.
    if (!this.loaded) return alert("ERROR: Movie is not loaded yet");
    var movie = document.getElementById('webcam_movie');
    if (!movie) { alert("ERROR: Cannot locate movie 'webcam_movie' in DOM"); }
    return movie;
  },

  set_stealth: function (stealth) {
    // Set or disable stealth mode.
    this.stealth = stealth;
  },

  snap: function (url, callback, stealth) {
    // Take snapshot and send to server.
    // Specify fully-qualified URL to server API script.
    // And callback function (string or function object).
    if (callback) { this.set_hook('onComplete', callback); }
    if (url) { this.set_api_url(url); }
    if ( typeof (stealth) != 'undefined') { this.set_stealth( stealth ); }
    this.get_movie()._snap( this.api_url, this.quality, this.shutter_sound ? 1 : 0, this.stealth ? 1 : 0 );
  },

  freeze: function () {
    // Freeze webcam image (capture but do not upload).
    this.get_movie()._snap('', this.quality, this.shutter_sound ? 1 : 0, 0 );
  },

  upload: function (url, callback) {
    // Upload image to server after taking snapshot.
    // Specify fully-qualified URL to server API script.
    // And callback function (string or function object).
    if (callback) { this.set_hook('onComplete', callback); }
    if (url) { this.set_api_url(url); }
    this.get_movie()._upload( this.api_url );
  },

  reset: function () {
    // Reset movie after taking snapshot.
    this.get_movie()._reset();
  },

  configure: function (panel) {
    // Open flash configuration panel -- specify tab name:.
    // "Camera", "privacy", "default", "localStorage", "microphone", "settingsManager".
    if (!panel) { panel = 'camera'; }
    this.get_movie()._configure(panel);
  },

  set_quality: function (new_quality) {
    // Set the JPEG quality (1 - 100).
    // Default is 90.
    this.quality = new_quality;
  },

  set_shutter_sound: function (enabled, url) {
    // Enable or disable the shutter sound effect.
    // Defaults to enabled.
    this.shutter_sound = enabled;
    this.shutter_url = url ? url : furl + 'shutter.mp3';
  },

  flash_notify: function (type, msg) {
    // Receive notification from flash about event.
    switch (type) {
      case 'flashLoadComplete':
        // Movie loaded successfully.
        this.loaded = true;
        this.fire_hook('onLoad');
        break;

      case 'error':
        // HTTP POST error most likely.
        if (!this.fire_hook('onError', msg)) {
          alert('JPEGCam Flash Error: ' + msg);
        }
        break;

      case 'success':
        // Upload complete, execute user callback function.
        // And pass raw API script results to function.
        this.fire_hook('onComplete', msg.toString());
        break;

      default:
        // Catch-all, just in case.
        alert('jpegcam flash_notify: ' + type + ': ' + msg);
        break;
    }
  }
};
