/*
 * @file
 * Webcam module js settings
 */
(function($) {
  Drupal.behaviors.webcam = {};
  Drupal.behaviors.webcam.attach = function(context) {
    $(window).load(function() {
      $(".webcam_player_markup").html(webcam.get_html(500, 500));
    });
    webcam.set_api_url( furl + 'webcam_snapshot.php?fname=sample' );
    webcam.set_quality( 90 ); // JPEG quality (1 - 100)
    webcam.set_shutter_sound( true ); // play shutter click sound
    webcam.set_hook( 'onComplete', 'my_completion_handler' );
  };
})(jQuery);

  function take_snapshot(fname){
    webcam.set_api_url( 'webcam/do_snapshot?fname=' + fname );
    // take snapshot and upload to server
    document.getElementById('upload_results').innerHTML = '<h1>Uploading...</h1>';
    webcam.snap();
  }
  function my_completion_handler(msg) {
    // extract URL out of PHP output
    if (msg == "Success") {
      // show JPEG image in page
      document.getElementById('upload_results').innerHTML ='<h1>Upload Successful!</h1>';
      // reset camera for another shot
      webcam.reset();
    }
    else {alert("PHP Error: " + msg); }
  }
